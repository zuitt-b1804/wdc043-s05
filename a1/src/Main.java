public class Main {
    public static void main(String[] args) {

        User newUser=new User("Tee Jae","Calinao",25,"Antipolo City");

        Course newCourse = new Course();
        newCourse.setName("Physics 101");
        newCourse.setDescription("Learn Physics");
        newCourse.setSeats(30);
        newCourse.setFee(5000.00);
        newCourse.setStartDate("May 2,2022");
        newCourse.setEndDate("May 31, 2022");
        newCourse.setInstructor(newUser);

        System.out.println("User's first name:");
        System.out.println(newUser.getFirstName());
        System.out.println("User's last name:");
        System.out.println(newUser.getLastName());
        System.out.println("User's age:");
        System.out.println(newUser.getAge());
        System.out.println("User's Address:");
        System.out.println(newUser.getAddress());

        System.out.println("Course's name:");
        System.out.println(newCourse.getName());
        System.out.println("Course's description");
        System.out.println(newCourse.getDescription());
        System.out.println("Course's Fee");
        System.out.println(newCourse.getFee());
        System.out.println("Course's Seats:");
        System.out.println(newCourse.getSeats());
        System.out.println("Course's instructor's first name:");
        System.out.println(newCourse.getInstructorName());

    }
}